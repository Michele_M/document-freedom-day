Japanese IPA fonts
==================

IPA Japanese fonts are available in Gnu/Linux package repositories.

* Homepage: http://ipafont.ipa.go.jp
* License: http://opensource.org/licenses/IPA
* Manual download: http://ipafont.ipa.go.jp/ipaexfont/IPAexfont00201.php

Fedora
------

Package names:

* ipa-ex-gothic-fonts
* ipa-ex-mincho-fonts
* ipa-gothic-fonts
* ipa-mincho-fonts
* ipa-pgothic-fonts
* ipa-pmincho-fonts